# Vaults

## enctry ( frist time )
ansible-vault encrypt --vault-id project@password_file secrets.yaml

## view content
ansible-vault view secrets.yaml --vault-password-file password_file

## edit content
ansible-vault edit secrets.yaml --vault-password-file password_file

EDITOR=nano ansible-vault edit secrets.yaml --vault-password-file password_file

---------------
ansible-vault encrypt_string --vault-password-file a_password_file 'foobar' --name 'the_secret'

ansible-vault encrypt_string --vault-id dev@a_password_file 'foooodev' --name 'the_dev_secret'

echo -n 'letmein' | ansible-vault encrypt_string --vault-id dev@a_password_file --stdin-name 'db_password'


ansible localhost -m ansible.builtin.debug -a var="new_user_password" -e "@vars.yml" --vault-id dev@a_password_file

ansible localhost -m ansible.builtin.debug -a var="db_password" -e "@vars.yml" --vault-id dev@a_password_file